package api

import (
	"encoding/json"
	"fmt"
	ds "jira-metric-service/data-structs"
	"net/url"
)

// GetQuery runs a query, returning the resulting list of JiraItems
func GetQuery(query string) []ds.JiraItem {
	response := Get(BaseQueryURL + url.QueryEscape(query))
	var jiList ds.JiraItemList
	err := json.Unmarshal(response, &jiList)
	if err == nil {
		return jiList.Issues
	}
	fmt.Println(err)
	return nil
}
