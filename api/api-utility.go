package api

import (
	"io/ioutil"
	"log"
	"net/http"
)

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Username to use for API requests
var Username string

// Password to use for API requests
var Password string

// BaseQueryURL is the base JIRA URL
var BaseQueryURL string

// Get submits a GET request to the url
func Get(query string) []byte {
	client := &http.Client{}
	req, err := http.NewRequest("GET", query, nil)
	checkError(err)
	req.SetBasicAuth(Username, Password)
	response, err := client.Do(req)
	checkError(err)
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err == nil {
		return bodyBytes
	}
	return nil
}
