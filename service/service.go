package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"jira-metric-service/api"
	ds "jira-metric-service/data-structs"
	db "jira-metric-service/database"
	"os"
	"strconv"
	"strings"
	"time"
)

type config struct {
	JiraUsername     string
	JiraPassword     string
	JiraBaseQueryURL string
	DBUsername       string
	DBPassword       string
	DBName           string
	DBHost           string
}

type metricDef struct {
	Query    string
	ID       int
	Desc     string
	Reinsert string
	Timer    string
}

var configuration config
var metricDefList []metricDef
var lastUpdateCache map[int]time.Time
var loggingLevel int

func containsID(m []metricDef, id int) bool {
	for _, me := range m {
		if me.ID == id {
			return true
		}
	}
	return false
}

// SetLoggingLevel sets the logging level of the service (for console logging)
func SetLoggingLevel(level int) {
	loggingLevel = level
}

// LoadConfig reads the config a configuration JSON file
func LoadConfig(filename string) bool {
	jsonFile, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Could not open specified config file '%v'\n", filename)
		return false
	}
	defer jsonFile.Close()
	byteData, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Printf("%v\n", err)
		return false
	}
	json.Unmarshal(byteData, &configuration)

	api.Username = configuration.JiraUsername
	api.Password = configuration.JiraPassword
	api.BaseQueryURL = configuration.JiraBaseQueryURL

	db.Username = configuration.DBUsername
	db.Password = configuration.DBPassword
	db.Name = configuration.DBName
	db.Host = configuration.DBHost

	fmt.Printf("Configuration loaded\n")
	fmt.Println()
	db.Connect()

	lastUpdateCache = make(map[int]time.Time)

	return true
}

// ParseMetricFile reads a JSON metric definition file
func ParseMetricFile(filename string) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Could not open specified JSON metric file '%v'\n", filename)
		return
	}
	defer jsonFile.Close()

	byteData, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Printf("%v\n", err)
		return
	}
	var tempList []metricDef
	json.Unmarshal(byteData, &tempList)
	fmt.Printf("Found %v metric definitions\n", len(tempList))
	for _, metric := range tempList {
		if !containsID(metricDefList, metric.ID) {
			metricDefList = append(metricDefList, metric)
		} else {
			fmt.Printf("!! ERROR adding definition !! Metric ID %v already found in metric definition list\n", metric.ID)
		}
	}
	fmt.Printf("Loaded %v metric definitions\n", len(metricDefList))
	fmt.Println()
}

// RunService starts the service running
func RunService() {
	fmt.Printf("Running service...\n")
	fmt.Println()
	for {
		fmt.Printf("Beginning metric cycle...\n")
		runQueries()
		// Sleep for 1 minute before repeating the cycle
		time.Sleep(60 * 1000 * time.Millisecond)
	}
}

func runQueries() {
	countNew := 0
	countReinsert := 0
	for _, metric := range metricDefList {
		nextRunTime, ready := checkLastRunTime(metric)
		if ready {
			fmt.Printf("Running metric ID %v, Query = \"%v\"\n", metric.ID, metric.Query)
			items := api.GetQuery(metric.Query)
			if items != nil {
				fmt.Printf("Query returned %v items:\n", len(items))
				for _, item := range items {
					if loggingLevel == 1 {
						fmt.Printf("%v\n", item)
					}
					rval := processMetricItem(metric, item)
					if rval == 0 {
						countNew++
					} else if rval == 1 {
						countReinsert++
					}
				}
			} else {
				fmt.Printf("Query returned 0 items (result was NIL)\n")
			}
		} else {
			fmt.Printf("Metric ID %v will run again at %v\n", metric.ID, nextRunTime)
		}

		fmt.Printf("%v new rows inserted\n", countNew)
		fmt.Printf("%v rows re-inserted\n", countReinsert)

		countNew = 0
		countReinsert = 0

		fmt.Println()
	}
}

func parseDuration(duration string) time.Duration {
	result := time.Hour * 8
	if len(duration) < 2 {
		return result
	}
	timestep := duration[len(duration)-1:]
	amount, err := strconv.Atoi(duration[:len(duration)-1])
	timeamt := 1
	if err == nil {
		timeamt = amount
	}
	switch strings.ToUpper(timestep) {
	case "H":
		result = time.Duration(timeamt) * time.Hour
	case "M":
		result = time.Duration(timeamt) * time.Minute
	case "S":
		result = time.Duration(timeamt) * time.Second
	}
	return result
}

func checkLastRunTime(metric metricDef) (time.Time, bool) {
	lastUpdate, isCached := lastUpdateCache[metric.ID]
	if isCached {
		// for now, default to 8h
		nextRunTime := lastUpdate.Add(parseDuration(metric.Timer))
		if time.Now().UTC().Before(nextRunTime) {
			return nextRunTime, false
		}
	}
	lastUpdateCache[metric.ID] = time.Now().UTC()
	return time.Now().UTC(), true
}

func processMetricItem(metric metricDef, item ds.JiraItem) int {
	rval := -1
	// WE can get away with only doing one FindItemIDWithCondition check here...
	existingID := db.FindItemID(metric.ID, item.Key, item.Fields.Assignee.Name)
	if existingID == -1 {
		err := db.WriteMetricItemToDatabase(metric.ID, item)
		if err != nil {
			fmt.Printf("!! ERROR inserting item !! %v\n", err)
			rval = -1
		} else {
			if loggingLevel == 1 {
				fmt.Printf("NEW ROW INSERTED\n")
			}
			rval = 0
		}
	} else {
		if loggingLevel == 1 {
			fmt.Printf("MATCHING ROW FOUND\n")
		}
		// Now, check if this metric allows updating
		rval = -1
		if len(metric.Reinsert) > 0 {
			condition := replaceCondition(metric.Reinsert, item)
			existingID := db.FindItemIDWithCondition(metric.ID, item.Key, item.Fields.Assignee.Name, condition)
			if existingID > -1 {
				// We have a match with the condition, reinsert
				err := db.WriteMetricItemToDatabase(metric.ID, item)
				if err != nil {
					fmt.Printf("!! ERROR inserting item !! %v\n", err)
					rval = -1
				} else {
					if loggingLevel == 1 {
						fmt.Printf("NEW ROW RE-INSERTED\n")
					}
					rval = 1
				}
			}
		}
	}
	return rval
}

func replaceCondition(condition string, item ds.JiraItem) string {
	parts := strings.Split(condition, " ")
	var newParts []string
	for _, part := range parts {
		if part[0] == '{' && part[len(part)-1] == '}' {
			field := part[1 : len(part)-1]
			switch field {
			case "Timespent":
				newParts = append(newParts, strconv.FormatInt(item.Fields.Timespent, 10))
			default:
				fmt.Printf("!! ERROR parsing condition !! %v\n", condition)
				return ""
			}
		} else {
			newParts = append(newParts, part)
		}
	}
	return strings.Join(newParts, " ")
}
