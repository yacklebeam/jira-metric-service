# Jira-Metric-Service

This application runs specified queries against a JIRA instance.

## Config
To set up, create two files in the root folder.

**config.json**
```
{
    "jirausername":"username",
    "jirapassword":"password",
    "jirabasequeryurl":"https://my-jira-url/api/search?jql=",
    "dbuser":"dbuser",
    "dbpass":"pass",
    "dbname":"metric-data",
    "dbhost":"localhost"
}
```

Since this tool is for running JQL queries, the baseQueryURL should end in `search?jql=` as of May 2019.

**metrics.json**
```
[
    {
        "id":1,
        "query":"ASSIGNEE=john.smith"
    },
    {
        "id":2,
        "query":"..."
    }
]
```

JQL queries which need embedded quotes should use single quotes.

## Running
The jira-metric-service will load the config and metric info, then run the specified queries in order.  As of May 2019, the resulting item lists are printed to the console.