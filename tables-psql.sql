-- postgresql tables

CREATE TABLE metric_items (
    id SERIAL,
    metric_id INTEGER NOT NULL,
    key VARCHAR(20) NOT NULL,
    sprint VARCHAR(20),
    assignee VARCHAR(100),
    time_spent INTEGER DEFAULT 0,
    time_estimated INTEGER DEFAULT 0,
    datetime_insert TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

GRANT SELECT,UPDATE,INSERT on metric_items TO metric;
GRANT USAGE ON metric_items_id_seq TO metric;