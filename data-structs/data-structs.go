package datastructs

import "fmt"

type assignee struct {
	Name string
}

type fields struct {
	Assignee             assignee
	Timespent            int64
	Timeoriginalestimate int64
}

// JiraItem is a single Jira Item
type JiraItem struct {
	Key    string
	Fields fields
}

// JiraItemList is a response from the query endpoint
type JiraItemList struct {
	StartAt    int
	MaxResults int
	Total      int
	Issues     []JiraItem
}

func (ji JiraItem) String() string {
	return fmt.Sprintf("{ Key: %v, Fields: %v }", ji.Key, ji.Fields)
}

func (f fields) String() string {
	return fmt.Sprintf("{ Assignee: %v, Timespent: %v, OriginalEstimate: %v }", f.Assignee, f.Timespent, f.Timeoriginalestimate)
}

func (a assignee) String() string {
	return fmt.Sprintf("%v", a.Name)
}
