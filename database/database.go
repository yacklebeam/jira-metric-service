package database

import (
	"database/sql"
	"fmt"
	ds "jira-metric-service/data-structs"

	// This needs to be included as a blank import
	_ "github.com/lib/pq"
)

// Username is the login name for the DB
var Username string

// Password is the login password
var Password string

// Name is the name of the database to connect to
var Name string

// Host is the address/hostname of the database server
var Host string

var db *sql.DB

// Connect uses the configuration info to connect to the database
func Connect() {
	connStr := "user=" + Username + " password=" + Password + " dbname=" + Name + " host=" + Host + " sslmode=disable"
	dbTemp, err := sql.Open("postgres", connStr)
	if err != nil {
		fmt.Printf("!! ERROR opening DB connection !! %v\n", err)
	} else {
		fmt.Printf("Connected to database '%v'\n", Name)
	}
	db = dbTemp
}

// WriteMetricItemToDatabase writes the metric item to the database
func WriteMetricItemToDatabase(metricID int, item ds.JiraItem) error {
	queryString := "INSERT INTO metric_items (metric_id, key, sprint, assignee, time_spent, time_estimated) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
	var ID int
	err := db.QueryRow(queryString, metricID, item.Key, nil, item.Fields.Assignee.Name, item.Fields.Timespent, item.Fields.Timeoriginalestimate).Scan(&ID)
	return err
}

// FindItemID returns the ID of the record matching the given metricID, key, and assignee, or -1 if not found
func FindItemID(metricID int, key string, assignee string) int {
	queryString := "SELECT id FROM metric_items WHERE metric_id = $1 AND key = $2 AND assignee = $3"
	ID := -1
	_ = db.QueryRow(queryString, metricID, key, assignee).Scan(&ID)
	return ID
}

// FindItemIDWithCondition returns the ID of the record matching the given metricID, key, and assignee AND the specififed condition or -1 if not found
func FindItemIDWithCondition(metricID int, key string, assignee string, condition string) int {
	queryString := "SELECT id FROM metric_items WHERE metric_id = $1 AND key = $2 AND assignee = $3"
	if len(condition) > 0 {
		queryString += " AND " + condition
	}
	ID := -1
	_ = db.QueryRow(queryString, metricID, key, assignee).Scan(&ID)
	return ID
}
