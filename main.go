package main

import (
	"jira-metric-service/service"
	"os"
	"strconv"
)

type config struct {
	loggingLevel int
}

var conf config

func parseOsArgs(args []string) {
	if len(args) > 1 {
		for i, arg := range args {
			if i == 0 {
				continue
			}
			if arg == "-l" {
				if len(args) > i+1 {
					logLevel, err := strconv.Atoi(args[i+1])
					if err == nil {
						conf.loggingLevel = logLevel
					} else {
						conf.loggingLevel = 0
					}
				}
			}
		}
	}
}

func main() {
	parseOsArgs(os.Args)
	service.ParseMetricFile("metrics.json")
	service.LoadConfig("config.json")
	service.SetLoggingLevel(conf.loggingLevel)
	service.RunService()
}
